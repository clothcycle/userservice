package com.javatechie.service;
import com.javatechie.model.User;
import com.javatechie.repository.UserRepository;
import com.javatechie.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAddUser() {
        User user = new User();
        user.setUserId(UUID.randomUUID().toString().split("-")[0]);

        when(userRepository.save(Mockito.any(User.class))).thenReturn(user);

        User savedUser = userService.addUser(new User());

        assertNotNull(savedUser);
        assertSame(user, savedUser);
    }

    @Test
    public void testFindAllUsers() {
        List<User> userList = new ArrayList<>();
        when(userRepository.findAll()).thenReturn(userList);

        List<User> users = userService.findAllUsers();

        assertNotNull(users);
        assertEquals(userList, users);
    }

    @Test
    public void testGetUserByUserId() {
        String userId = "someUserId";
        User user = new User();
        user.setUserId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        User retrievedUser = userService.getUserByUserId(userId);

        assertNotNull(retrievedUser);
        assertEquals(userId, retrievedUser.getUserId());
    }

    @Test
    public void testGetUserByEmail() {
        String userEmail = "user@example.com";
        User user = new User();
        user.setEmail(userEmail);

        // Adjust this part based on the actual return type of findByEmail
        when(userRepository.findByEmail(userEmail)).thenReturn(user);

        User retrievedUser = userService.getUserByEmail(userEmail);

        assertEquals(userEmail, retrievedUser.getEmail());
    }

    @Test
    public void testUpdateUser() {
        User userRequest = new User();
        userRequest.setUserId("someUserId");

        User existingUser = new User();
        existingUser.setUserId("someUserId");

        when(userRepository.findById("someUserId")).thenReturn(Optional.of(existingUser));
        when(userRepository.save(Mockito.any(User.class))).thenReturn(existingUser);

        User updatedUser = userService.updateUser(userRequest);

        assertNotNull(updatedUser);
        assertSame(existingUser, updatedUser);
    }

    @Test
    public void testDeleteUser() {
        String userId = "someUserId";

        String result = userService.deleteUser(userId);

        assertNotNull(result);
        assertEquals(userId + " user deleted from the database.", result);
    }
}
