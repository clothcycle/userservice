package com.javatechie.repository;
import com.javatechie.model.User;
import com.javatechie.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {

        userRepository.deleteAll();
    }

    @Test
    public void testFindByFirstName() {
        User user = new User();
        user.setFirstName("ikram");
        user.setLastName("belaid");
        user.setEmail("john.ikram@example.com");
        userRepository.save(user);

        List<User> foundUsers = userRepository.findByFirstName("ikram");

        assertEquals(1, foundUsers.size());

    }

    @Test
    public void testFindByLastName() {
        User user = new User();
        user.setFirstName("Jane");
        user.setLastName("Smith");
        user.setEmail("jane.smith@example.com");
        userRepository.save(user);

        List<User> foundUsers = userRepository.findByLastName("Smith");

        assertEquals(1, foundUsers.size());

    }

    @Test
    public void testFindByEmail() {
        User user = new User();
        user.setFirstName("Alice");
        user.setLastName("Johnson");
        user.setEmail("alice.johnson@example.com");
        userRepository.save(user);

        User foundUser = userRepository.findByEmail("alice.johnson@example.com");

        assertEquals("alice.johnson@example.com", foundUser.getEmail());
    }
}
