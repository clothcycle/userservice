package com.javatechie.service;

import com.javatechie.model.User;
import com.javatechie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User addUser(User user) {
        user.setUserId(UUID.randomUUID().toString().split("-")[0]);
        return userRepository.save(user);
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public User getUserByUserId(String userId) {
        return userRepository.findById(userId).get();
    }

    public User updateUser(User userRequest) {
        User existingUser = userRepository.findById(userRequest.getUserId()).get();
        existingUser.setEmail(userRequest.getEmail());
        existingUser.setPassword(userRequest.getPassword());
        existingUser.setAddress(userRequest.getAddress());
        existingUser.setFirstName(userRequest.getFirstName());
        existingUser.setLastName(userRequest.getLastName());
        existingUser.setBio(userRequest.getBio());
        existingUser.setRating(userRequest.getRating());
        existingUser.setProfilePicture(userRequest.getProfilePicture());
        return userRepository.save(existingUser);
    }

    public String deleteUser(String userId) {
        userRepository.deleteById(userId);
        return userId + " user deleted from the database.";
    }
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}